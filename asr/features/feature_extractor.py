import math
from typing import List, Tuple

import torch

from asr.model.device import Device
from fairseq.checkpoint_utils import load_model_ensemble_and_task
from fairseq.models.wav2vec import ConvFeatureExtractionModel


class FeatureExtractor:
    def __init__(self, feature_extractor: ConvFeatureExtractionModel, conv_feature_layers: List[Tuple[int, int, int]],
                 device: Device):
        self._feature_extractor = feature_extractor
        self._conv_feature_layers = conv_feature_layers
        self._device = device

        self._feature_extractor = self._device.to(self._feature_extractor)

    @classmethod
    def load(cls, checkpoint: str, device: Device) -> 'FeatureExtractor':
        model, cfg, task = load_model_ensemble_and_task([checkpoint])
        model = model[0]
        model.eval()
        feature_extractor = model.w2v_encoder.w2v_model.feature_extractor

        conv_layers = eval(cfg['model'].conv_feature_layers)

        return cls(feature_extractor, conv_layers, device)

    def forward(self, source: torch.Tensor) -> torch.Tensor:
        with torch.no_grad():
            source = self._device.to(source)
            return self._feature_extractor(torch.unsqueeze(source, 0)).squeeze(0).cpu().numpy()

    def get_output_length(self, input_length: int) -> int:

        def conv_out_length(length, kernel_size, stride):
            return math.floor((length - kernel_size) / stride + 1)

        for i in range(len(self._conv_feature_layers)):
            input_length = conv_out_length(input_length, self._conv_feature_layers[i][1],
                                           self._conv_feature_layers[i][2])

        return input_length
