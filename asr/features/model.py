from dataclasses import dataclass

from fairseq.models import register_model
from fairseq.models.wav2vec.wav2vec2 import Wav2Vec2Config, Wav2Vec2Model


@dataclass
class Wav2Vec2FeaturesConfig(Wav2Vec2Config):
    pass


@register_model("wav2vec2_features", dataclass=Wav2Vec2FeaturesConfig)
class Wav2Vec2FeaturesModel(Wav2Vec2Model):
    def __init__(self, cfg: Wav2Vec2FeaturesConfig):
        super().__init__(cfg)

    def forward(
        self, source, padding_mask=None, mask=True, features_only=False,
        mask_indices=None, mask_channel_indices=None,
        padding_count=None,
    ):
        features = source.transpose(1, 2)
        features = self.layer_norm(features)

        if self.post_extract_proj is not None:
            features = self.post_extract_proj(features)

        features = self.dropout_input(features)

        if mask:
            x, mask_indices = self.apply_mask(
                features, padding_mask,
                mask_indices=mask_indices,
                mask_channel_indices=mask_channel_indices,
            )
        else:
            x = features

        x = self.encoder(x, padding_mask=padding_mask)
        return {"x": x, "padding_mask": padding_mask}
