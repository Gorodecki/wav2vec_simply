import argparse

import torch


def main():
    parser = argparse.ArgumentParser(description='Prepares a pre-trained checkpoint for the feature-based learning. '
                                                 'Replaces wav2vec2 model by wav2vec2_features')
    parser.add_argument('--checkpoint', type=str, required=True, help='Path to the checkpoint')
    parser.add_argument('--output', type=str, required=True, help='Path to the updated checkpoint')
    args = parser.parse_args()

    state = torch.load(args.checkpoint)
    state['cfg']['model']['_name'] = 'wav2vec2_features'
    torch.save(state, args.output)


if __name__ == '__main__':
    main()
