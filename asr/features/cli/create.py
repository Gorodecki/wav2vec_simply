import argparse
import logging
import os

import h5py
import torch
from tqdm import tqdm

from asr.data.dataset import Dataset
from asr.data.manifest import Manifest
from asr.features.feature_extractor import FeatureExtractor
from asr.model.device import Device
from asr.utils.files import replace_extension

MANIFEST_EXTENSION = '.tsv'
FEATURES_SUBDIRECTORY = 'files'
FEATURES_FILE_EXTENSION = '.hdf5'
SAMPLE_RATE = 16_000


def main():
    logging.basicConfig(level=logging.DEBUG)
    args = _parse_arguments()

    logging.info('Loading the checkpoint')
    feature_extractor = FeatureExtractor.load(args.checkpoint, Device(args.gpu))

    logging.info('Processing files')
    split, _ = os.path.splitext(os.path.basename(args.manifest))
    features_root = os.path.join(args.output, FEATURES_SUBDIRECTORY)

    max_sample_size = Manifest.find_max_sample_size(args.manifest)
    logging.info('Calculating the maximum sample size: %d', max_sample_size)

    with Manifest(args.manifest) as original_manifest, \
            open(os.path.join(args.output, split + MANIFEST_EXTENSION), 'w') as features_manifest:

        print(features_root, file=features_manifest)
        for sample in tqdm(Dataset(original_manifest)):
            features_filename = replace_extension(sample.filename, FEATURES_FILE_EXTENSION)
            features_path = os.path.join(features_root, features_filename)

            print(f'{features_filename}\t{sample.frames}', file=features_manifest)
            if os.path.exists(features_path):
                continue

            data = _pad_sample(sample.source, max_sample_size)
            features = feature_extractor.forward(data)
            output_length = feature_extractor.get_output_length(sample.frames)
            padding_mask = torch.zeros(features.shape[-1], dtype=torch.bool)
            padding_mask[output_length:] = True

            with h5py.File(features_path, 'w') as features_file:
                features_file['size'] = output_length
                features_file['sample'] = features
                features_file['padding_mask'] = padding_mask

    logging.info('All files have been successfully processed')


def _parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Transform a dataset consisting of audio files into a dataset '
                                                 'consisting of features created by the feature extractor')
    parser.add_argument('--manifest', type=str, help='Path to the original manifest (fairseq)', required=True)
    parser.add_argument('--output', type=str, help='Path to a directory where the transformed dataset will be stored',
                        required=True)
    parser.add_argument('--checkpoint', type=str, help='Path to the pre-trained model checkpoint', required=True)
    parser.add_argument("--gpu", help="GPU to use", type=int)
    args = parser.parse_args()

    if not os.path.exists(args.manifest):
        logging.error('The original manifest does not exist')
        exit(1)

    try:
        os.makedirs(os.path.join(args.output, FEATURES_SUBDIRECTORY), exist_ok=True)
    except Exception as e:
        logging.exception('The output directory creating failure: %s', e)
        exit(1)

    return args


def _pad_sample(sample: torch.Tensor, max_sample_size: int) -> torch.Tensor:
    assert len(sample.shape) == 1

    diff = max_sample_size - len(sample)
    if diff > 0:
        sample = torch.cat([sample, sample.new_full((diff,), 0.0)])
    elif diff < 0:
        sample = sample[:max_sample_size]

    return sample


if __name__ == '__main__':
    main()
