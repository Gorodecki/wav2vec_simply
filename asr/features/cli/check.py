import argparse
import types

import numpy as np
import torch
from tqdm import tqdm

from asr.data.dataset import Dataset
from asr.data.manifest import Manifest
from asr.features.dataset import FeatureDataset
from asr.features.model import Wav2Vec2FeaturesModel
from asr.model import Encoder, extract_dictionary
from asr.model.device import Device
from fairseq.checkpoint_utils import load_checkpoint_to_cpu


def main():
    parser = argparse.ArgumentParser('Compare the feature based encoder and the original one')
    parser.add_argument('--original-manifest', type=str, help='Path to the original manifest')
    parser.add_argument('--feature-manifest', type=str, help='Path to the features manifest')
    parser.add_argument('--checkpoint', type=str, required=True, help='Path to the checkpoint')
    parser.add_argument('--gpu', type=int, help='GPU to use')
    args = parser.parse_args()

    state = load_checkpoint_to_cpu(args.checkpoint)
    dictionary = extract_dictionary(state)
    device = Device(args.gpu)
    original_encoder = Encoder(state, dictionary, device)
    feature_encoder = _create_feature_encoder(state, dictionary, device)

    feature_dataset = FeatureDataset(args.feature_manifest)

    diff = []
    with Manifest(args.original_manifest) as manifest:
        for i, sample in tqdm(enumerate(Dataset(manifest))):
            original_emissions = original_encoder.forward(sample.source).numpy()
            with torch.no_grad():
                net_input = feature_dataset.collater([feature_dataset[i]])['net_input']
                if args.gpu is not None:
                    net_input['source'] = net_input['source'].cuda(args.gpu)
                    net_input['padding_mask'] = net_input['padding_mask'].cuda(args.gpu)

                feature_emissions = feature_encoder.get_logits(feature_encoder.forward(**net_input)).cpu().numpy()

            close = np.allclose(original_emissions, feature_emissions, atol=1e-5)
            if not close:
                d = np.absolute(original_emissions - feature_emissions).max()
                diff.append((sample.filename, d))

    if len(diff) > 0:
        print('For the following files the produced emissions differ:')
        for filename, d in diff:
            print(filename, d)
        print(f'Summary: {len(diff)} of {len(feature_dataset)} are different')
    else:
        print('All corresponding emissions are close to each other')


def _create_feature_encoder(state, dictionary, device: Device):
    encoder = Encoder(state, dictionary, device)
    model = encoder._model
    model.w2v_encoder.w2v_model.forward = types.MethodType(Wav2Vec2FeaturesModel.forward, model.w2v_encoder.w2v_model)
    return model
