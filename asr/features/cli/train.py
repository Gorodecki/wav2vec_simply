import fairseq_cli.hydra_train

from fairseq.dataclass.configs import FairseqConfig


def custom_pre_main(cfg: FairseqConfig):
    import asr.features.task  # noqa

    pre_main(cfg)


pre_main = fairseq_cli.hydra_train.pre_main
fairseq_cli.hydra_train.pre_main = custom_pre_main
main = fairseq_cli.hydra_train.cli_main


if __name__ == '__main__':
    main()
