import os

import h5py
import numpy as np
import pyarrow
import torch

from asr.data.manifest import Manifest
from fairseq.data.fairseq_dataset import FairseqDataset


class FeatureDataset(FairseqDataset):
    def __init__(self, manifest_path, shuffle=True):
        super().__init__()

        self.shuffle = shuffle
        with Manifest(manifest_path) as manifest:
            self.root_dir = manifest.root
            fnames = []
            sizes = []
            for filename, size in manifest:
                fnames.append(filename)
                sizes.append(size)

        self.sizes = np.array(sizes, dtype=np.int64)
        self.fnames = pyarrow.array(fnames)

    def __getitem__(self, index):
        path = os.path.join(self.root_dir, str(self.fnames[index]))
        with h5py.File(path, 'r') as features:
            return {
                'id': index,
                'source': np.array(features['sample'], dtype=np.float64),
                'padding_mask': np.array(features['padding_mask'], dtype=np.bool),
                'size': np.array(features['size'])
            }

    def __len__(self):
        return len(self.sizes)

    def collater(self, samples):
        samples = [s for s in samples if s["source"] is not None]
        if len(samples) == 0:
            return {}

        max_sample_size = max(s['size'] for s in samples)
        sources = [s['source'][:, :max_sample_size] for s in samples]
        padding_masks = [s['padding_mask'][:max_sample_size] for s in samples]

        return {
            'id': torch.LongTensor([s["id"] for s in samples]),
            'net_input': {
                'source': torch.from_numpy(np.stack(sources)).float(),
                'padding_mask': torch.from_numpy(np.stack(padding_masks))
            }
        }

    def num_tokens(self, index):
        return self.size(index)

    def size(self, index):
        return self.sizes[index]
