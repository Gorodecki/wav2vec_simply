import logging
import os

from fairseq.data import AddTargetDataset
from fairseq.dataclass import FairseqDataclass
from fairseq.tasks import register_task
from fairseq.tasks.audio_pretraining import AudioPretrainingConfig, AudioPretrainingTask, LabelEncoder

from .dataset import FeatureDataset
from .model import Wav2Vec2FeaturesModel  # noqa To add the model to the model registry

logger = logging.getLogger(__name__)


@register_task("features_fine_tuning", dataclass=AudioPretrainingConfig)
class FeaturesFineTuningTask(AudioPretrainingTask):
    def __init__(self, cfg: AudioPretrainingConfig):
        super().__init__(cfg)

    def load_dataset(self, split: str, task_cfg: FairseqDataclass = None, **kwargs):
        data_path = self.cfg.data
        task_cfg = task_cfg or self.cfg

        self.datasets[split] = FeatureDataset(manifest_path=os.path.join(data_path, "{}.tsv".format(split)))
        with open(os.path.join(data_path, f"{split}.{task_cfg.labels}"), "r") as f:
            labels = [line for i, line in enumerate(f)]

        assert len(labels) == len(self.datasets[split]), (
            f"labels length ({len(labels)}) and dataset length "
            f"({len(self.datasets[split])}) do not match"
        )

        self.datasets[split] = AddTargetDataset(
            self.datasets[split],
            labels,
            pad=self.target_dictionary.pad(),
            eos=self.target_dictionary.eos(),
            batch_targets=True,
            process_label=LabelEncoder(self.target_dictionary),
            add_to_input=False,
        )

        logger.info('%s dataset has been loaded: %d samples', split, len(self.datasets[split]))
