import os
from dataclasses import dataclass

import soundfile as sf
import torch
import torch.nn.functional as F

from .manifest import Manifest


def normalize(source: torch.Tensor):
    """
    Applies normalization to the sample. See fairseq/data/audio/raw_audio_dataset.py:62
    :param source: sample
    :return: normalized sample
    """
    with torch.no_grad():
        source = F.layer_norm(source, source.shape)

    return source


@dataclass
class Sample:
    filename: str
    frames: int
    source: torch.Tensor

    @classmethod
    def read(cls, filename: str) -> 'Sample':
        source, _ = sf.read(filename)
        source = torch.from_numpy(source).float()
        # Assume that the sample will be used with a model that uses normalized samples
        source = normalize(source)
        return cls(filename, len(source), source)


class Dataset:
    def __init__(self, manifest: Manifest):
        self._manifest = manifest

    def __iter__(self):
        return self

    def __next__(self) -> Sample:
        filename, frames = next(self._manifest)
        sample = Sample.read(os.path.join(self._manifest.root, filename))
        sample.filename = filename

        assert sample.frames == frames, 'The number of frames in the loaded file is not equal to the number in the ' \
                                        'manifest'
        return sample
