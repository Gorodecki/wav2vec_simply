import os
from dataclasses import dataclass
from typing import Dict, List, Optional

import soundfile


@dataclass
class Sample:
    file: str
    transcription: str
    frames: Optional[int] = None


@dataclass
class Split:
    name: str
    samples: List[Sample]
    root: str
    max_sample_size: Optional[int] = None

    def __post_init__(self):
        for sample in self.samples:
            sample.frames = soundfile.info(os.path.join(self.root, sample.file)).frames

        if self.max_sample_size is not None:
            samples_len = len(self.samples)
            self.samples = [s for s in self.samples if s.frames <= self.max_sample_size]
            print(f'Removed {samples_len - len(self.samples)} samples whose size is bigger than `max_sample_size`')

    def save_manifest(self, destination: str):
        with open(os.path.join(destination, self.name + '.tsv'), 'w') as f:
            print(self.root, file=f)
            for sample in self.samples:
                print(f'{sample.file}\t{sample.frames}', file=f)

    def save_transcriptions(self, destination: str):
        with open(os.path.join(destination, self.name + '.wrd'), 'w') as wrd_f, \
                open(os.path.join(destination, self.name + '.ltr'), 'w') as ltr_f:
            for sample in self.samples:
                letters = ' '.join(list(sample.transcription.replace(' ', '|'))) + ' |'
                print(sample.transcription, file=wrd_f)
                print(letters, file=ltr_f)

    def build_dictionary(self) -> Dict[str, int]:
        dictionary: Dict[str, int] = {}
        for sample in self.samples:
            transcription = sample.transcription.replace(' ', '|')
            for c in transcription:
                dictionary[c] = dictionary.get(c, 0) + 1

        return dictionary

    def save_dictionary(self, destination: str):
        dictionary = self.build_dictionary()
        with open(os.path.join(destination, 'dict.ltr.txt'), 'w') as f:
            for character in sorted(dictionary.keys()):
                print(f'{character} {dictionary[character]}', file=f)
