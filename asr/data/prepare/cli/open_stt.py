import argparse

from asr.data.prepare.split import Sample, Split
from asr.utils.files import find_root
from asr.utils.validators import validate_path


def main():
    parser = argparse.ArgumentParser(description='Create the manifest and label files suitable for training wav2vec 2.0'
                                                 ' using fairseq')
    parser.add_argument('sources', type=str, help='Path to the source Open STT', nargs='+')
    parser.add_argument('--destination', type=validate_path, required=True,
                        help='Path to a directory where the processed data will be stored')
    parser.add_argument('--max-sample-size', type=int, help='Maximum length of a sample file', default=600_000)
    args = parser.parse_args()

    files = []
    transcriptions = []
    for source in args.sources:
        with open(source, 'r') as f:
            next(f)
            for row in f:
                transcription, file = row.strip().split(',')
                files.append(file)
                transcriptions.append(transcription)

    root = find_root(files)
    for i in range(len(files)):
        files[i] = files[i][len(root)+1:]

    for i in range(len(transcriptions)):
        transcriptions[i] = transcriptions[i].upper()

    train_split = Split('train', [Sample(f, t) for f, t in zip(files, transcriptions)], root, args.max_sample_size)
    print(f'Open STT dataset. Train set: {len(train_split.samples)}.')
    print(f'Dictionary: {train_split.build_dictionary()}')

    train_split.save_manifest(args.destination)
    train_split.save_transcriptions(args.destination)


if __name__ == '__main__':
    main()
