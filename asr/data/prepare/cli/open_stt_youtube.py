import argparse
import re

from asr.data.prepare.split import Sample, Split
from asr.utils.files import find_root

chars_to_ignore_regex = r'[,?.!\\\-;:"“%‘”«»…\'()‑–—−]'


def main():
    parser = argparse.ArgumentParser(description='Create the manifest and label files suitable for training wav2vec 2.0'
                                                 ' using fairseq')
    parser.add_argument('source', type=str, help='Path to the source Open STT')
    parser.add_argument('destination', type=str,
                        help='Path to a directory where the processed data will be stored')
    parser.add_argument('--max-sample-size', type=int, help='Maximum length of a sample file', default=600_000)
    parser.add_argument('--subset', type=str, help='Name of the subset', default='train')
    args = parser.parse_args()

    files = []
    transcriptions = []
    with open(args.source, 'r') as f:
        next(f)
        for row in f:
            file, _, transcription, *_ = row.strip().split(',')
            files.append(file)
            transcriptions.append(transcription)

    root = find_root(files)
    for i in range(len(files)):
        files[i] = files[i][len(root)+1:]

    for i in range(len(transcriptions)):
        transcriptions[i] = re.sub(chars_to_ignore_regex, '', transcriptions[i]).upper()

    split = Split(args.subset, [Sample(f, t) for f, t in zip(files, transcriptions)], root, args.max_sample_size)
    print(f'Open STT dataset. {args.subset} set: {len(split.samples)}.')
    print(f'Dictionary: {split.build_dictionary()}')

    split.save_manifest(args.destination)
    split.save_transcriptions(args.destination)


if __name__ == '__main__':
    main()
