import argparse
import os
import re
import string
import subprocess
from typing import List

from tqdm import tqdm

from asr.data.prepare.split import Sample, Split
from asr.utils.files import replace_extension
from asr.utils.validators import validate_path

chars_to_ignore_regex = r'[,?.!\\\-;:"“%‘”«»…\'()‑–—−]'

TRAIN_SPLIT_FILES = ['train.tsv', 'dev.tsv']
TEST_SPLIT_FILES = ['test.tsv']
FILES_DIR = 'clips'


def main():
    parser = argparse.ArgumentParser(description='Create the manifest and label files suitable for training wav2vec 2.0'
                                                 ' using fairseq')
    parser.add_argument('source', type=validate_path, help='Path to the Common Voice dataset')
    parser.add_argument('destination', type=validate_path, help='Path to a directory where the processed data will be '
                                                                'stored')
    args = parser.parse_args()

    convert_samples_to_flac(os.path.join(args.source, FILES_DIR), os.path.join(args.destination, FILES_DIR))

    train_samples = read_split(args.source, TRAIN_SPLIT_FILES)
    train_split = Split('train', train_samples, os.path.join(args.destination, FILES_DIR))
    train_split.save_manifest(args.destination)
    train_split.save_transcriptions(args.destination)
    train_split.save_dictionary(args.destination)

    test_samples = read_split(args.source, TEST_SPLIT_FILES)
    test_split = Split('test', test_samples, os.path.join(args.destination, FILES_DIR))
    test_split.save_manifest(args.destination)
    test_split.save_transcriptions(args.destination)

    print(f'Common Voice dataset. Train set: {len(train_samples)}. Test set: {len(test_samples)}')
    print(f'Dictionary: {train_split.build_dictionary()}')


def read_samples(filename: str) -> List[Sample]:
    samples: List[Sample] = []
    with open(filename, 'r') as f:
        _ = next(f)
        for row in f:
            _, path, sentence, *_ = row.split('\t')
            path = replace_extension(path, '.flac')
            words = re.sub(chars_to_ignore_regex, '', sentence).upper()
            words = words.replace('Ё', 'Е')
            if all(c not in string.ascii_uppercase for c in sentence):
                samples.append(Sample(path, words))

    return samples


def read_split(root: str, filenames: List[str]) -> List[Sample]:
    return sum([read_samples(os.path.join(root, filename)) for filename in filenames], [])


def convert_samples_to_flac(source_dir: str, destination_dir: str):
    os.makedirs(destination_dir, exist_ok=True)

    for source_file in tqdm(os.listdir(source_dir)):
        base_name, extension = os.path.splitext(source_file)
        if extension == '.mp3':
            destination_file = base_name + '.flac'
            destination_path = os.path.join(destination_dir, destination_file)
            if os.path.exists(destination_path):
                continue

            process = subprocess.run(['ffmpeg', '-y', '-i', os.path.join(source_dir, source_file), '-ac', '1', '-ar',
                                      '16000', destination_path], capture_output=True)
            if process.returncode != 0:
                print(f'WARNING: {source_file} conversion to FLAC has failed')


if __name__ == '__main__':
    main()
