import argparse
import json
import os
from typing import List

from asr.data.prepare.split import Sample, Split
from asr.utils.validators import validate_path


def main():
    parser = argparse.ArgumentParser(description='Create the manifest and label files suitable for training wav2vec 2.0'
                                                 ' using fairseq')
    parser.add_argument('--source', type=str, help='Path to the source file')
    parser.add_argument('--files', type=validate_path, required=True, help='Path to a directory containing audio files')
    parser.add_argument('--destination', type=validate_path, required=True,
                        help='Path to a directory where the processed data will be stored')
    parser.add_argument('--max-sample-size', type=int, help='Maximum length of a sample file', default=600_000)
    parser.add_argument('--split', type=str, help='The name of the split', default='test')
    args = parser.parse_args()

    samples: List[Sample] = []
    with open(args.source, 'r') as f:
        for row in f:
            record = json.loads(row)
            samples.append(Sample(os.path.basename(record['audio_filepath']), record['text'].upper()))

    split = Split(args.split, samples, args.files, args.max_sample_size)
    print(f'Golden dataset: {len(split.samples)}.')
    print(f'Dictionary: {split.build_dictionary()}. Dictionary size: {len(split.build_dictionary())}')

    split.save_manifest(args.destination)
    split.save_transcriptions(args.destination)


if __name__ == '__main__':
    main()
