class Manifest:
    def __init__(self, filename: str):
        self._file = open(filename)
        self.root = next(self._file).strip()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._file.close()

    def __iter__(self):
        return self

    def __next__(self):
        filename, frames = next(self._file).strip().split('\t')
        return filename, int(frames)

    @staticmethod
    def find_max_sample_size(filename: str) -> int:
        with Manifest(filename) as manifest:
            return max(size for _, size in manifest)
