from typing import Optional, Union

import torch


class Device:
    def __init__(self, gpu: Optional[int] = None):
        self._gpu = gpu

    def to(self, target: Union[torch.Tensor, torch.nn.Module]):
        if self._gpu is not None:
            return target.cuda(torch.device(f'cuda:{self._gpu}'))
        else:
            return target.cpu()
