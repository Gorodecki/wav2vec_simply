import itertools
import math
from argparse import ArgumentParser, Namespace
from typing import Dict, Type

import torch

from fairseq.data.data_utils import post_process
from fairseq.data.dictionary import Dictionary


class Decoder:
    decoders: Dict[str, Type['Decoder']] = {}

    def __init__(self, dictionary: Dictionary, **kwargs):
        self.dictionary = dictionary

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)

        name = cls.__name__.replace('Decoder', '').lower()
        cls.decoders[name] = cls

    def decode(self, emissions: torch.Tensor) -> str:
        """
        Decode the encoder's output to a sentence
        :param emissions: A tensor with the encoder's output with shape (1, <length>).
        Where <length> depends on the input file length
        :return: hypothesis
        """
        raise NotImplementedError


class GreedyDecoder(Decoder):
    def __init__(self, dictionary: Dictionary, **kwargs):
        super().__init__(dictionary, **kwargs)

        self._blank = self.dictionary.index(
            "<ctc_blank>") if "<ctc_blank>" in self.dictionary.indices else self.dictionary.bos()

    def decode(self, emissions: torch.Tensor) -> str:
        indexes = filter(lambda x: x != self._blank,
                         (g[0] for g in itertools.groupby(emissions.squeeze().argmax(dim=1))))

        hyp_pieces = self.dictionary.string(torch.LongTensor(list(indexes)).int().cpu())
        return post_process(hyp_pieces, 'letter')


class FairseqDecoderMixin:
    def decode(self, emissions: torch.Tensor) -> str:
        hypo = self._decoder.decode(emissions.transpose(0, 1).float().cpu().contiguous())  # type: ignore
        hyp_pieces = self.dictionary.string(hypo[0][0]['tokens'].int().cpu())  # type: ignore
        return post_process(hyp_pieces, 'letter')


class ViterbiDecoder(FairseqDecoderMixin, Decoder):
    def __init__(self, dictionary: Dictionary, **kwargs):
        super().__init__(dictionary, **kwargs)

        from collections import namedtuple

        from examples.speech_recognition.w2l_decoder import W2lViterbiDecoder

        decoder_args = namedtuple('args', 'criterion nbest')('ctc', 1)  # type: ignore
        self._decoder = W2lViterbiDecoder(decoder_args, self.dictionary)


class KenlmDecoder(FairseqDecoderMixin, Decoder):
    def __init__(self, dictionary: Dictionary, args: Namespace, **kwargs):
        super().__init__(dictionary, **kwargs)

        from examples.speech_recognition.w2l_decoder import W2lKenLMDecoder

        args.unit_lm = args.lexicon is None
        args.beam = 50
        args.beam_threshold = 25
        args.lm_weight = 0.2
        args.word_score = 1.0
        args.unk_weight = -math.inf
        args.sil_weight = 0.0
        args.criterion = 'ctc'
        args.nbest = 1

        self._decoder = W2lKenLMDecoder(args=args, tgt_dict=dictionary)


def add_kenlm_args(parser: ArgumentParser):
    group = parser.add_argument_group('KenLM')
    group.add_argument('--lexicon', help='Path to the lexicon')
    group.add_argument('--kenlm-model', help='Path to the language model')
