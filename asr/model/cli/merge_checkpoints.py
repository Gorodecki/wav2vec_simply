import argparse

import torch


def main():
    parser = argparse.ArgumentParser(description='Add information about the pre-trained model to the checkpoint')
    parser.add_argument('--fine-tuned', type=str, required=True, help='Path to the fine-tuned checkpoint')
    parser.add_argument('--pre-trained', type=str, required=True, help='Path to the pre-trained checkpoint')
    parser.add_argument('--merged', type=str, required=True, help='Path to the merged checkpoint')
    args = parser.parse_args()

    fine_tuned = torch.load(args.fine_tuned)
    pre_trained = torch.load(args.pre_trained)
    fine_tuned['cfg']['model']['w2v_args'] = pre_trained['cfg']
    torch.save(fine_tuned, args.merged)


if __name__ == '__main__':
    main()
