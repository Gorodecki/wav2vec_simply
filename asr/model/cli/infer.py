import argparse

from asr.data.dataset import Sample
from asr.model import Decoder, Encoder, Model, extract_dictionary
from asr.model.decoder import add_kenlm_args
from asr.model.device import Device
from fairseq.checkpoint_utils import load_checkpoint_to_cpu


def main():
    parser = argparse.ArgumentParser(description='Convert a sound file containing speech to text')
    parser.add_argument('source', type=str, help='Path to the sound file')
    parser.add_argument('--checkpoint', type=str, required=True, help='Path to the checkpoint')
    parser.add_argument('--gpu', type=int, help='GPU to use')
    parser.add_argument('--decoder', type=str, choices=list(Decoder.decoders))
    add_kenlm_args(parser)
    args = parser.parse_args()

    state = load_checkpoint_to_cpu(args.checkpoint)
    dictionary = extract_dictionary(state)
    encoder = Encoder(state, dictionary, Device(args.gpu))
    if args.decoder is None:
        args.decoder = list(Decoder.decoders.keys())[0]
    decoder = Decoder.decoders[args.decoder](dictionary, args=args)
    model = Model(encoder, decoder)

    sample = Sample.read(args.source)
    sentence = model.infer(sample.source)
    print(sentence)


if __name__ == '__main__':
    main()
