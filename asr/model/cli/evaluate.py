import argparse
import os
from typing import Generator

from jiwer import wer
from tqdm import tqdm

from asr.data.dataset import Dataset, Sample
from asr.data.manifest import Manifest
from asr.model import Decoder, Encoder, extract_dictionary
from asr.model.decoder import add_kenlm_args
from asr.model.device import Device
from fairseq.checkpoint_utils import load_checkpoint_to_cpu


def main():
    parser = argparse.ArgumentParser(description='Evaluate a fine-tuned model')
    parser.add_argument('data_dir', type=str, help='Path to the directory containing manifest and ground truth files')
    parser.add_argument('--split', type=str, required=True, help='Name of split')
    parser.add_argument('--checkpoint', type=str, required=True, help='Path to the checkpoint')
    parser.add_argument('--gpu', type=int, help='GPU to use')
    parser.add_argument('--decoders', type=str, choices=list(Decoder.decoders), required=True, nargs='+')
    add_kenlm_args(parser)
    args = parser.parse_args()

    state = load_checkpoint_to_cpu(args.checkpoint)
    dictionary = extract_dictionary(state)
    encoder = Encoder(state, dictionary, Device(args.gpu))

    args.decoders = list(set(args.decoders))
    decoders = [Decoder.decoders[d](dictionary, args=args) for d in args.decoders]
    wers = [[] for _ in decoders]

    for sample, transcription in tqdm(zip(read_samples(args.data_dir, args.split),
                                          read_transcriptions(args.data_dir, args.split))):
        emissions = encoder.forward(sample.source)
        hypos = [d.decode(emissions) for d in decoders]

        for i, hypo in enumerate(hypos):
            wers[i].append(wer(transcription, hypo))

    print('Average WER:')
    for i, decoder in enumerate(args.decoders):
        print(f'{decoder}: {average(wers[i])}')
    print('')


def read_samples(data_dir: str, split: str) -> Generator[Sample, None, None]:
    with Manifest(os.path.join(data_dir, f'{split}.tsv')) as manifest:
        for sample in Dataset(manifest):
            yield sample


def read_transcriptions(data_dir: str, split: str) -> Generator[str, None, None]:
    with open(os.path.join(data_dir, f'{split}.wrd')) as transcriptions:
        for row in transcriptions:
            yield row.strip()


def average(data: list) -> float:
    return sum(data) / len(data)


if __name__ == '__main__':
    main()
