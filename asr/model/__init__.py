from typing import Any, Dict

import torch

from fairseq.data.dictionary import Dictionary

from .decoder import Decoder
from .encoder import Encoder


class Model:
    def __init__(self, encoder: Encoder, decoder: Decoder):
        self._encoder = encoder
        self._decoder = decoder

    def infer(self, data: torch.Tensor) -> str:
        emissions = self._encoder.forward(data)
        return self._decoder.decode(emissions)


def extract_dictionary(state: Dict[str, Any]) -> Dictionary:
    return state['task_state']['target_dictionary']
