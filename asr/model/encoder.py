from typing import Any, Dict

import torch

from fairseq.data.dictionary import Dictionary
from fairseq.models.wav2vec import Wav2VecCtc, Wav2VecEncoder

from .device import Device


class Encoder:
    """
    Implements a fine-tuned Wav2vec 2.0 model.
    It's assumed that the model was fine-tuned with following parameters:
    - normalize = True
    - pad = True
    - sample_rate = 16KHz
    """

    def __init__(self, state: Dict[str, Any], dictionary: Dictionary, device: Device):
        encoder = Wav2VecEncoder(state['cfg'].model, dictionary)
        self._model = Wav2VecCtc(state['cfg'].model, encoder)  # noqa
        self._model.load_state_dict(state['model'], strict=True)
        self._model.eval()

        self._device = device
        self._model = self._device.to(self._model)

    def forward(self, source: torch.Tensor) -> torch.Tensor:
        assert source.dim() == 1, 'Batches are not supported'

        with torch.no_grad():
            source = torch.unsqueeze(source, 0)
            source = self._device.to(source)
            padding_mask = torch.zeros(source.shape, dtype=torch.bool)

            return self._model.get_logits(self._model(source=source, padding_mask=padding_mask)).float().cpu()
