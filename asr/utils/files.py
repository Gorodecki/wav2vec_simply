import os
from typing import List


def find_root(files: List[str]):
    root_parts = files[0].split('/')
    rest = files[1:]
    end = 0
    while end < len(rest):
        if any(not f.startswith('/'.join(root_parts[:end+1])) for f in rest):
            break
        end += 1

    return '/'.join(root_parts[:end])


def replace_extension(filename: str, extension: str) -> str:
    return os.path.splitext(filename)[0] + extension
