import os


def validate_path(value: str) -> str:
    try:
        os.makedirs(value, exist_ok=True)
    except Exception:
        raise ValueError

    return value
