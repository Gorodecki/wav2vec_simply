# Wav2vec
A set of tools that simplifies Wav2vec 2.0 usage

## Installation

0. Update submodules
1. Initialize virtualenv and install dependencies: `pipenv install`
2. Install `PyTorch`
3. Install `fairseq`
4. Install `flashlight` python bindings

For clean server initialization use [the guide](docs/server_intialization_guide.md)

## Training

```bash
fairseq-hydra-train \
    task.data={data-path} \
    model.w2v_path={checkpoint-path} \
    distributed_training.distributed_world_size={gpu-count} \
    +optimization.update_freq='[{24 / {gpu-count}]' \
    --config-dir fairseq/examples/wav2vec/config/finetuning \
    --config-name {config-name}
```

## Evaluation

```bash
python fairseq/examples/speech_recognition/infer.py \
    {data-path} \
    --task audio_pretraining \
    --nbest 1 \
    --path {checkpoint-path} \
    --gen-subset {subset} \
    --results-path {results-path} \
    --w2l-decoder viterbi \
    --lm-weight 2 \
    --word-score 1 \
    --sil-weight 0 \
    --criterion ctc \
    --labels ltr \
    --max-tokens 4000000 \
    --post-process letter
```

## Inference

```bash
infer {audio-file-path} --checkpoint {checkpoint-path} --gpu {gpu-index} --decoder {decoder-name}
```

## Decoders comparison

```bash
evaluate {data-path} --split {split-name} --checkpoint {checkpoint-path} --gpu {gpu-index} --decoders {decoders-list}
```