# Installation

The guide describes how to install the package and its dependencies onto a server with no internet connection (a ssh reverse tunnel is used to provide the internet connection)

## Initialize the environment

1. Install python packages and Pipenv

```bash
sudo apt install python3-dev python3-venv python3-pip pipenv
```

2. Clone the repository

```bash
git clone git@gitlab.com:Gorodecki/wav2vec_simply.git
cd wav2vec
```

3. Install local dependencies:

```bash
export PIPENV_VENV_IN_PROJECT=1
pipenv install
```

4. Install Nvidia [drivers](https://linuxize.com/post/how-to-nvidia-drivers-on-ubuntu-20-04/#installing-the-nvidia-drivers-using-the-command-line) :

```bash
sudo apt install nvidia-driver-460
sudo reboot
```

## Set the internet connection

1. Create a tunnel over SSH

```bash
ssh user@server_name -R 0.0.0.0:9050
```

2. Set SOCKS5 proxy:

```bash
export all_proxy=socks5://localhost:9050
```

3. Add the proxy to the git configuration (`.gitconfig`):

```bash
# Update .gitconfig
[http]
    proxy=socks5://127.0.0.1:9050
[https]
    proxy=socks5://127.0.0.1:9050
```

4. Install `tsocks` for using non-standard repositories:

```bash
sudo apt install tsocks
```

5. Update `tsocks` config file:

```
server = 127.0.0.1
# Server type defaults to 4 so we need to specify it as 5 for this one
server_type = 5
# The port defaults to 1080 but I've stated it here for clarity 
server_port = 9050
```

```bash
sudo apt install tsocks
```

## Install internet dependencies

1. Install [CUDA](https://developer.nvidia.com/cuda-11.2.0-download-archive?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=2004&target_type=deblocal)

```bash
cd /tmp
curl -LO https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
curl -LO https://developer.download.nvidia.com/compute/cuda/11.2.0/local_installers/cuda-repo-ubuntu2004-11-2-local_11.2.0-460.27.04-1_amd64.deb
sudo dpkg -i cuda-repo-ubuntu2004-11-2-local_11.2.0-460.27.04-1_amd64.deb
sudo apt-key add /var/cuda-repo-ubuntu2004-11-2-local/7fa2af80.pub
sudo apt-get update
sudo apt-get -y install cuda
export PATH=/usr/local/cuda-11.2/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-11.2/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
```

2. Install CuDNN
   
```bash
cd /tmp
sudo apt install software-properties-common
curl -OL https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin 
sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
curl -OL https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/7fa2af80.pub
sudo apt-key add 7fa2af80.pub
sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /"
sudo tsocks apt-get update
sudo tsocks apt-get install libcudnn8=8.1.1.33-1+cuda11.2
sudo tsocks apt-get install libcudnn8-dev=8.1.1.33-1+cuda11.2
```

3. Install Intel MKL according to the flashlight's [CI/CD](https://github.com/flashlight/flashlight/blob/5f10c8c4458305429fc6b88eb81ab5fbd2ce6a76/.circleci/config.yml#L264)

```bash
cd /tmp
curl -OL https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB
sudo apt-key add GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB
sudo sh -c 'echo deb https://apt.repos.intel.com/mkl all main > /etc/apt/sources.list.d/intel-mkl.list'
sudo tsocks apt-get update 
sudo tsocks apt install intel-mkl-64bit-2020.0-088
export LD_LIBRARY_PATH=/opt/intel/lib/intel64:/opt/intel/mkl/lib/intel64:${LD_LIBRARY_PATH}
export MKLROOT=/opt/intel/mkl
```

4. Install VCPKG

```bash
git clone https://github.com/microsoft/vcpkg
./vcpkg/bootstrap-vcpkg.sh
```

5. Install Flashlight and KenLM

```bash
./vcpkg/vcpkg install flashlight-cuda
./vcpkg/vcpkg install kenlm
```

6. Install Pytorch

```bash
cd wav2vec
pipenv shell
pip install torch==1.8.1+cu111 torchvision==0.9.1+cu111 torchaudio==0.8.1 -f https://download.pytorch.org/whl/torch_stable.html
```

7. Initialize submodules (fairseq, flashlight)
   
```bash
git submodule init
git submodule update
```

8. Install [fairseq](https://github.com/pytorch/fairseq)

```bash
cd fairseq
pip install --editable .
```

9. Install [Flashlight python bindings](https://github.com/facebookresearch/flashlight/tree/master/bindings/python)

```bash
sudo apt install cmake fftw3-dev
export CMAKE_LIBRARY_PATH=[vcpkg_installation_location]/installed/x64-linux/lib
export CMAKE_INCLUDE_PATH=[vcpkg_installation_location]/installed/x64-linux/include
cd ../flashlight/bindings/python
```

Modify CMakeLists.txt
```
add_definitions(-DTHRUST_IGNORE_CUB_VERSION_CHECK)
```

```bash
python setup.py install
```
